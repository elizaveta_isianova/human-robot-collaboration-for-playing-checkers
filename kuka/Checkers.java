package application;

import java.util.ArrayList;

import javax.inject.Inject;

import com.kuka.generated.ioAccess.PLC_ControlIOGroup;
import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.*;

import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.geometricModel.Frame;
import com.kuka.roboticsAPI.geometricModel.ObjectFrame;
import com.kuka.roboticsAPI.geometricModel.math.Transformation;
import com.kuka.roboticsAPI.motionModel.PTP;
import com.kuka.roboticsAPI.uiModel.ApplicationDialogType;

public class Checkers extends RoboticsAPIApplication {
    @Inject
    private LBR lbr;

    @Inject
    public PLC_ControlIOGroup io;

    @Inject
    private Moves moves;

    private int curState;

    private boolean endGame;
    private int piecesCaptLeft;


    public static int[][] boardIO;

    private boolean pickDone;
    private boolean placeDone;
    private boolean analyzeDone;
    private boolean standCheck;
    private boolean pickWhiteKing;
    private boolean captureKing;
    private boolean prevEaten;
    private boolean placingKingFrStack;


    private ArrayList<Integer> pickCoordX = new ArrayList<Integer>();
    private ArrayList<Integer> pickCoordY = new ArrayList<Integer>();
    private ArrayList<Integer> placeCoordX = new ArrayList<Integer>();
    private ArrayList<Integer> placeCoordY = new ArrayList<Integer>();

    private final static String informationText=
            "This application is intended for floor mounted robots!"+ "\n" +
                    "\n" +
                    "The robot will perform a couple of basic moves.";



    public void run() {
        initialize();
        while (!endGame) {
            listenToProfinet();
            curState = io.getState();
            switch (curState){
                case 0:
                    waitForStart();
                    break;

                case 1:

                    break;

                case 2:
                    stateTwo();
                    break;

                case 3:

                    break;

                case 4:
                    stateFour();
                    break;

                case 8:
                    stateEight();
                    break;

                case 7:
                    stateSeven();
                    break;

                case 9:
                    stateNine();
                    break;

                case 11:
                    stateEleven();
                    //endGame = true;
                    break;
                //case 12:
                	//moves.goHome(io.getSTART());

                default:

                    break;

            }
        }
        getLogger().info("GAME END");
        io.setPhotoPosDone(false);
        io.setPickDone(false);
        io.setPlaceDone(false);
    }

    private void stateTwo(){
        io.setACK(false);
        standCheck 		 = 	false;
        prevEaten		 = 	false;
        piecesCaptLeft	 = 	0;

        //ObjectFrame newframe = getApplicationData().getFrame("/PhotoPos");
        
        moves.goHome(io.getSTART());

        if (!moves.isAtHome){
            System.out.println("Returned to HOME. Is at home: " + moves.isAtHome);
        }
        moves.isAtHome = true;
        io.setPhotoPosDone(true);
        io.setAnalyzeDone(false);
    }


    private void stateFour(){
        if (analyzeDone){return;}

        io.setPhotoPosDone(false); 
        boardIO	  = readBoardIO();
        int diff = io.getDifficulty();
        
        boolean moveFound = Game.analizeBoard(boardIO, io.getSTART(), diff, io.getKukaStarts());

        if (Game.gameEnd){
        	
            io.setGameEnd(true);
            if (Game.KukaWins){
                io.setKukaWins(true);
            } else {
                io.setHumanWins(true);
            }
            
        } else {
            if (moveFound){
                piecesCaptLeft	= Game.piToCaptLeft;
                pickCoordX 		= Game.pickCoordX;
                pickCoordY 		= Game.pickCoordY;
                placeCoordX 	= Game.placeCoordX;
                placeCoordY 	= Game.placeCoordY;

                getLogger().info("Pieces to capture: " + piecesCaptLeft);

                io.setCapturePiecesAmount(piecesCaptLeft);
                io.setAnalyzeError(false);
                io.setAnalyzeDone(true);
                io.setWhitesLeft(Game.actWhites);


                analyzeDone = true;
            } else {
            	if (Game.moveWasMade){
            		io.setAnalyzeError(true);
            		
            	}
            	io.setAnalyzeDone(true);
            }
        }
    }


    private void stateSeven(){
        if (placeDone){return;}

        if (Game.turnIntoKing){
            Game.turnIntoKing = false;
            moves.placeRegToStack(io.getKukaStarts(), Game.actWhites);
            pickWhiteKing = true;

        } else {

            if (captureKing) {
            	Game.blackKings = Game.blackKings - 1;
                moves.placeKingToStack(io.getKukaStarts(), Game.blackKings);
                captureKing = false;
            } else {
                getLogger().info("Gonig to: X: " + placeCoordX.get(0) + ", Y: " + placeCoordY.get(0));
                int x = placeCoordX.get(0);
                int y = placeCoordY.get(0);

                if (moves.removePiece(x)){
                	prevEaten = true;
                    moves.placeEatenRegToStack(io.getKukaStarts(), Game.actBlacks);
                } else {
                	if (placingKingFrStack){
                		moves.standUp(Game.whiteKings*7.5 + 30);
                		placingKingFrStack = false;
                	}
                    moves.placeRegToBoard(x, y, Game.actBlacks, io.getKukaStarts());
                }

            }

            placeCoordX.remove(0);
            placeCoordY.remove(0);
        }

        io.setPlaceDone(true);
        io.setPickDone(false);
        placeDone = true;

        piecesCaptLeft = piecesCaptLeft - 1;
        getLogger().info("Left: " + piecesCaptLeft);
        io.setCapturePiecesAmount(piecesCaptLeft);

        pickDone = false;

    }


    private void stateEight(){
        io.setAnalyzeDone(false);

        analyzeDone = false;
        
        
        if (pickDone){return;}

        if (pickWhiteKing){
            moves.standUp(150);
            moves.pickKingFromStack(Game.whiteKings, io.getKukaStarts());
            placingKingFrStack = true;
            pickWhiteKing = false;
        } else {

            if (capturingKing(pickCoordX.get(0), pickCoordY.get(0))){
                captureKing = true;
            }

            getLogger().info("GO Pick true");
            getLogger().info("Gonig to: X: " + pickCoordX.get(0) + ", Y: " + pickCoordY.get(0));
            if (prevEaten){
            	moves.standUp(150);
            } else {
            	moves.standUp(40);
            }
            moves.pickFromBoard(pickCoordX.get(0), pickCoordY.get(0));

            pickCoordX.remove(0);
            pickCoordY.remove(0);
        }
        io.setPickDone(true);
        moves.isAtHome = false;

        io.setCapturePiecesAmount(piecesCaptLeft);
        pickDone = true;
    }

    private void stateNine(){
        io.setPlaceDone(false);
        placeDone = false;
    }


    private void stateEleven(){
        if (!standCheck){
            standCheck = true;
            io.setACK(true);
        }
        io.setWhitesLeft(Game.actWhites);
        io.setBlacksLeft(Game.actBlacks);

    }


    public void initialize() {

        curState 		= 	0;
        piecesCaptLeft  = 	0;
        pickDone 		= 	false;
        placeDone 		= 	false;
        analyzeDone 	= 	false;
        standCheck 		= 	false;
        pickWhiteKing 	= 	false;
        captureKing		=	false;
        endGame  		=	false;
        prevEaten		= 	false;
        placingKingFrStack = false;

        io.setPhotoPosDone(false);
        io.setPickDone(false);
        io.setPlaceDone(false);
        io.setAnalyzeDone(false);
        io.setAnalyzeError(false);
        io.setHumanWins(false);
        io.setKukaWins(false);
        io.setGameEnd(false);
        io.setCapturePiecesAmount(0);

        /*
        Game.turnIntoKing = false;
		Game.actBlacks	  = 12;
		Game.actWhites 	  = 12;
		*/

        Game.initGame();

        io.setWhitesLeft(Game.actWhites);
        io.setWhitesLeft(Game.actBlacks);

        moves.initializeImp();
        moves.isAtHome = false;

		/*
		io.setACK(true);
		while (true){
			if (io.getSTART()) { break; }
		}
		io.setACK(false);
		*/
    }

    public void listenToProfinet(){
        if (io.getResetGame()){
            curState = 0;
            piecesCaptLeft= 	0;
            pickDone 		= 	false;
            placeDone 		= 	false;
            analyzeDone 	= 	false;
            standCheck 		= 	false;
            pickWhiteKing 	= 	false;
            captureKing		=	false;
            prevEaten		= 	false;
            placingKingFrStack = false;

            pickCoordX.clear();
            pickCoordY.clear();
            placeCoordX.clear();
            placeCoordY.clear();

            io.setPhotoPosDone(false);
            io.setPickDone(false);
            io.setPlaceDone(false);
            io.setAnalyzeDone(false);
            io.setAnalyzeError(false);
            io.setHumanWins(false);
            io.setKukaWins(false);
            io.setGameEnd(false);
            io.setCapturePiecesAmount(0);

            Game.initGame();

            io.setWhitesLeft(Game.actWhites);
            io.setWhitesLeft(Game.actBlacks);


            
            moves.initializeImp();


        }
    }

    private int[][] readBoardIO(){
        int board[][] = new int[8][8];
        board[0][1] = io.getPos01();
        board[0][3] = io.getPos03();
        board[0][5] = io.getPos05();
        board[0][7] = io.getPos07();

        board[1][0] = io.getPos10();
        board[1][2] = io.getPos12();
        board[1][4] = io.getPos14();
        board[1][6] = io.getPos16();

        board[2][1] = io.getPos21();
        board[2][3] = io.getPos23();
        board[2][5] = io.getPos25();
        board[2][7] = io.getPos27();

        board[3][0] = io.getPos30();
        board[3][2] = io.getPos32();
        board[3][4] = io.getPos34();
        board[3][6] = io.getPos36();

        board[4][1] = io.getPos41();
        board[4][3] = io.getPos43();
        board[4][5] = io.getPos45();
        board[4][7] = io.getPos47();

        board[5][0] = io.getPos50();
        board[5][2] = io.getPos52();
        board[5][4] = io.getPos54();
        board[5][6] = io.getPos56();

        board[6][1] = io.getPos61();
        board[6][3] = io.getPos63();
        board[6][5] = io.getPos65();
        board[6][7] = io.getPos67();

        board[7][0] = io.getPos70();
        board[7][2] = io.getPos72();
        board[7][4] = io.getPos74();
        board[7][6] = io.getPos76();

        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (((col % 2) + 2) % 2 != ((((row +  1) % 2) + 2) % 2)) {
                    board[row][col] = 0;
                }
            }
        }

        return board;
    }


    private boolean capturingKing(int x, int y){
        if (boardIO[x][y] == 3 && io.getKukaStarts()){
            return true;
        }

        if (boardIO[x][y] == 4 && !io.getKukaStarts()){
            return true;
        }

        return false;
    }


    private void waitForStart(){
        io.setACK(true);
        while (true){
            if (io.getSTART()) { break; }
        }
        io.setACK(false);
    }

}
