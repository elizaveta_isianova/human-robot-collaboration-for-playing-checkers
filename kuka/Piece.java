package application;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;


public class Piece {
    public int xPos = 0;
    public int yPos = 0;
    public String Color = "-";
    public boolean king = false;
    public boolean upDirection = false;
    public boolean activeStatus = false;
    public LinkedList<Piece> validPath = new LinkedList<Piece>();
    public ArrayList<Piece> validMoves = new ArrayList<Piece>();
    public ArrayList<Piece> visitedCells = new ArrayList<Piece>();
    public Piece bestMove;
    public int Score;

    private final int[] xArr = {1, -1};
    private final int[] yArr = {1, -1};


    Piece(int xPos, int yPos, String Color, boolean king, boolean upDirection, boolean activeStatus, LinkedList<Piece> validPath){
        this.xPos = xPos;
        this.yPos = yPos;
        this.Color = Color;
        this.king = king;
        this.upDirection = upDirection;
        this.activeStatus = activeStatus;
        this.validPath = validPath;

    }

    public void changePiece(int x, int y, String col, boolean q, boolean upDir, boolean activeSt){
        xPos = x;
        yPos = y;
        Color = col;
        king = q;
        upDirection = upDir;
        activeStatus = activeSt;

    }

    public void makeO(){
        Color = "o";
        king = false;
        upDirection = false;
        activeStatus = true;
    }
    public void makeX(){
        Color = "x";
        king = false;
        upDirection = true;
        activeStatus = true;
    }


    public int retXPos(){
        return xPos;
    }

    public void make_king(){
        king = true;

    }

    public void removePiece(){
        activeStatus = false;
        upDirection = false;
        Color = " ";
        king = false;

    }

    public boolean findValidMoves(Board b, int x, int y, boolean start, String KUKA_color){

        boolean moveFound = false;

        if (notWithingBoundaries(x,y)){ return false;}

        boolean empty = isEmpty(b, x, y);

        if (closeToEdge(x,y,start)){
            if (empty && !validMoves.contains(b.board[x][y])){
                moveFound = addValidMove(b, x, y);
            }
        }


        if (king){
            if (!visitedCells.contains(b.board[x][y])) {
                visitedCells.add(b.board[x][y]);
            }

            boolean moveFoundLeftTop = checkValidMoves(b, x, y, start, xArr[1], yArr[1], KUKA_color);

            boolean moveFoundRightTop = checkValidMoves(b, x, y, start, xArr[1], yArr[0], KUKA_color);

            boolean moveFoundLeftBottom = checkValidMoves(b, x, y, start, xArr[0], yArr[1], KUKA_color);

            boolean moveFoundRightBottom = checkValidMoves(b, x, y, start,  xArr[0], yArr[0], KUKA_color);

            if (moveFoundLeftTop || moveFoundRightTop || moveFoundLeftBottom || moveFoundRightBottom){
                moveFound = true;
            }
        } else {
            int xDir = chooseDir();

            boolean moveFoundLeft = checkValidMoves(b, x, y, start, xDir, yArr[1], KUKA_color);

            boolean moveFoundRight = checkValidMoves(b, x, y, start, xDir, yArr[0], KUKA_color);

            if (moveFoundRight || moveFoundLeft){
                moveFound = true;
            }
        }

        if (empty && !moveFound) {
            moveFound = addValidMove(b, x, y);
        }
        return moveFound;
    }

    public boolean findValidPath(Board b, int x, int y, boolean start, int xGoal, int yGoal, String KUKA_color){
        boolean endReached = false;

        if (notWithingBoundaries(x,y)){return false;}

        if ((x == xGoal) && (y == yGoal)){return true;}

        String HumanColor = chooseHumCol(KUKA_color);
        if (b.board[x][y].Color == HumanColor){ return false;}

        if (king) {
            for (int i=0; i<=1; i++){
                for (int j=0; j<=1;j++){
                    int xDir = xArr[i];
                    int yDir = yArr[j];
                    if (notWithingBoundaries(x+xDir, y+yDir)){ continue;}
                    if (!visitedCells.contains(b.board[x+xDir][y+yDir])) {
                        visitedCells.add(b.board[x + xDir][y + yDir]);
                        endReached = checkValidPath(b, x, y, start, xDir, yDir, xGoal, yGoal, endReached, KUKA_color);
                        if (endReached) {return endReached;}
                    }
                }
            }
        } else {
            int xDir = chooseDir();
            for (int j=0; j<=1;j++){
                int yDir = yArr[j];
                if (notWithingBoundaries(x+xDir, y+yDir)){ continue;}
                if (!visitedCells.contains(b.board[x+xDir][y+yDir])) {
                    visitedCells.add(b.board[x + xDir][y + yDir]);
                    endReached = checkValidPath(b, x, y, start, xDir, yDir, xGoal, yGoal, endReached, KUKA_color);
                    if (endReached) {return endReached;}
                }
            }
        }

        //endReached = closeToEdge(x,y,start); #TODO: maybe uncomment it later

        return endReached;
    }

    public boolean checkValidMoves(Board b, int x, int y, boolean start, int xDir, int yDir, String KUKA_color){

        boolean moveFound = false;
        String HumanColor = chooseHumCol(KUKA_color);

        if (start && ((xDir==-1 && x==1) || (xDir==1 && x==6) || (yDir ==-1 && y==1) || (yDir == 1 && y==6))){
            int newX = x+xDir;
            int newY = y+yDir;
            if (newX <=7 && newX>=0 && newY>=0 && newY<=7){
                if (isEmpty(b, newX, newY) ){
                    moveFound = addValidMove(b, newX, newY);
                }
            }
        }

        if (notWithinValidBound(x,y,xDir,yDir)){return false;}

        if (notWithingBoundaries(x,y)) {return false;} // #TODO: maybe delete later

        boolean empty = isEmpty(b, x, y);

        if (start && isEmpty(b, x+xDir, y+yDir)){
            //boolean
            moveFound = addValidMove(b, x+xDir, y+yDir); // #TODO it was changed here
        }
        /*
        if (start && ((yDir==-1 && y>=1) || (yDir==1 && y<=6)) && ((x>=1 && xDir==-1) || (x<=6 && xDir==1))){
            if (board[x+xDir][y+yDir].Color == " ") {
                boolean res = addValidMove(board, x+xDir, y+yDir);
            }
        }

         */
        if ( !notWithingBoundaries(x+2*xDir, y+2*yDir) && b.board[x+xDir][y+yDir].Color == HumanColor && isEmpty(b, x+2*xDir, y+2*yDir)){
            if (!visitedCells.contains(b.board[x+2*xDir][y+2*yDir])) {
                moveFound = findValidMoves(b, x + 2 * xDir, y + 2 * yDir, false, KUKA_color);
                if (empty && !moveFound && !validMoves.contains(b.board[x][y])) {
                    moveFound = addValidMove(b, x, y);
                    if (moveFound) {
                        visitedCells.add(b.board[x + 2*xDir][y + 2*yDir]);
                    }
                }
            }
        }
        return moveFound;
    }

    public boolean checkValidPath(Board b, int x, int y, boolean start, int xDir, int yDir, int xGoal, int yGoal, boolean endReached, String KUKA_color){
        String HumanColor = chooseHumCol(KUKA_color);

        if (visitedCells.contains(b.board[x][y])){return false;}

        if (notWithinValidBound(x,y,xDir,yDir)){ return false;}

        boolean empty = isEmpty(b, x, y);

        if (start) {
            if (goalReached(x+xDir,y+yDir,xGoal,yGoal) && isEmpty(b,x+xDir, y+yDir)){ return true;}
        }
        if (b.board[x + xDir][y + yDir].Color == HumanColor) {
            endReached = findValidPath(b, x +2*xDir, y + 2*yDir, false, xGoal, yGoal, KUKA_color);
            if (empty || start ) {
                if (endReached && !validPath.contains(b.board[x+xDir][y+yDir])) {
                    validPath.add(b.board[x + xDir][y + yDir]);
                    return endReached;
                }
            }
        }
        return endReached;
    }

    public boolean addValidMove(Board b, int xCoord, int yCoord) {
        boolean found = false;
        if (king){
            if (!validMoves.contains(b.board[xCoord][yCoord]) ){
                validMoves.add(b.board[xCoord][yCoord]);
                found = true;   // #TODO: it wasnt here before
            }
        } else if ((upDirection && bestMove.xPos >= xCoord) || (!upDirection && bestMove.xPos <= xCoord)){
            validMoves.add(b.board[xCoord][yCoord]);
            bestMove = b.board[xCoord][yCoord];
            //removeWrongMoves();
            removeWrongMovesValidation();
            found = true;
        }
        return found;
    }

    public void calculateScore(Board b, String KukaColor){
        /*
        if (!king) {
            if (validPath.size() == 0 && validMoves.size() == 0) { Score = 0;}
            else {
                removeWrongMovesValidation();
                boolean vP = findValidPath(b, xPos, yPos, true, bestMove.xPos, bestMove.yPos, KukaColor);
                Score = validPath.size() + 1;
            }
        } else {


         */
        if (validMoves.size() == 0){
            Score = 0;
            return;
        }
        bestMove = validMoves.get(0);
        LinkedList<Piece> bestPath = new LinkedList<Piece>();
        Score = 0;
        for(int i=0; i<validMoves.size(); i++){
            visitedCells.clear();
            validPath.clear();
            boolean vP = findValidPath( b,xPos, yPos,  true, validMoves.get(i).xPos, validMoves.get(i).yPos, KukaColor);
            if ((validPath.size() + 1) > Score){
                Score = validPath.size() + 1;
                bestMove = validMoves.get(i);
                bestPath.clear();
                for (int j=0;j<validPath.size();j++){
                    bestPath.add(validPath.get(j));
                }
            }
        }
        validPath = bestPath;
    }

    private int chooseDir(){
        int xDir = 1;
        if (upDirection){
            xDir = -1;
        }
        return xDir;
    }

    private boolean closeToEdge(int x, int y, boolean start){
        if (!start && ((x>=0 && x <= 1) || ( x>=6 && x<=7))) {
            if (((y>=0) && (y<=1)) || ((y>=6) && (y<=7))){
                return true;
            }
        }
        return false;
    }

    private boolean isEmpty(Board b, int x, int y){
        if (b.board[x][y].Color == " ") {
            return true;
        }
        return false;
    }

    private boolean notWithingBoundaries(int x, int y){
        if ((x<0 || x>7 || y<0 || y>7)){
            return true;
        }
        return false;
    }

    private boolean notWithinValidBound(int x, int y, int xDir, int yDir){
        if ((xDir==1 && x>=6) || (xDir==-1 && x<=1) || (yDir==1 && y>=6) || (yDir== -1 && y<=1)){
            return true;
        }
        return false;
    }

    private boolean goalReached(int x, int y, int xGoal, int yGoal){
        if ((x == xGoal) && (y == yGoal)){
            return true;
        }
        return false;
    }

    public  String chooseHumCol(String KUKA_color){
        String HumanColor = "x";
        if (KUKA_color == HumanColor){
            HumanColor = "o";
        }
        return HumanColor;
    }

    public void removeWrongMoves(){

        if (upDirection){
            for (Piece i : validMoves) {
                if (i.xPos < bestMove.xPos){
                    validMoves.remove(i);
                }
            }
        } else {
            for (Piece i : validMoves) {
                if (i.xPos > bestMove.xPos){
                    validMoves.remove(i);
                }
            }
        }
    }

    public static Comparator<Piece> xDown = new Comparator<Piece>() {

        public int compare(Piece s1, Piece s2) {

            int rollno1 = s1.xPos;
            int rollno2 = s2.xPos;



            //For descending order
            return rollno2-rollno1;
        }
    };

    public static Comparator<Piece> xUp = new Comparator<Piece>() {

        public int compare(Piece s1, Piece s2) {

            int rollno1 = s1.xPos;
            int rollno2 = s2.xPos;

            //for ascending order
            return rollno1-rollno2;

        }
    };

    public void removeWrongMovesValidation(){
        if (validMoves.size() == 0) { return;}
        ArrayList<Piece> trueMoves = new ArrayList<Piece>();

        if (upDirection){
            Collections.sort(validMoves, xUp);
            bestMove = validMoves.get(0);
            trueMoves.add(bestMove);
            for (Piece i : validMoves) {
                if (i.xPos <= bestMove.xPos){
                    trueMoves.add(i);
                }
            }
        } else {
            Collections.sort(validMoves, xDown);
            bestMove = validMoves.get(0);
            trueMoves.add(bestMove);
            for (Piece i : validMoves) {
                if (i.xPos >= bestMove.xPos){
                    trueMoves.add(i);
                }
            }
        }
        validMoves.clear();
        for (Piece j : trueMoves){
            if (!validMoves.contains(j)){
                validMoves.add(j);
            }
        }
    }

    public boolean onEdge(){
        return yPos == 0 || yPos == 7;
    }
    
    
    public boolean becomesKing(){
    	if (xPos == 0 || xPos == 7){
    		return true;
    	}
    	return false;
    }


}
