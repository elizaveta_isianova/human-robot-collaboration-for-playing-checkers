package application;
import java.util.*;

public class Board {
    public  Piece[][] board = createBoard();
    public  ArrayList<Piece> posMovesStart = new ArrayList<Piece>();
    public  ArrayList<Piece> posMovesEnd = new ArrayList<Piece>();

    public void possibleMoves(String curColor){
        posMovesStart.clear();
        posMovesEnd.clear();
        for (int row=0; row<8; row++) {
            for (int col = 0; col < 8; col++) {
                if (board[row][col].Color == curColor) {
                    Piece curPiece = board[row][col];
                    calcPieceScore(curPiece, curColor);
                    for ( Piece j : curPiece.validMoves){
                        posMovesStart.add(curPiece);
                        posMovesEnd.add(j);
                    }
                }
            }
        }
        boolean greaterScFound 		= false;
        ArrayList<Piece> tmpStart 	= new ArrayList<Piece>();
        ArrayList<Piece> tmpEnd 	= new ArrayList<Piece>();
        for (int i = 0; i < posMovesStart.size(); i++){
            Piece start = posMovesStart.get(i);
            Piece end = posMovesEnd.get(i);
            start.visitedCells.clear();
            start.validPath.clear();
            boolean vP = start.findValidPath(this, start.xPos, start.yPos, true, end.xPos, end.yPos, curColor);
            start.Score = start.validPath.size();
            int Score = start.Score;
            
            if (greaterScFound){
            	if (Score >= 1){
            		tmpStart.add(start);
            		tmpEnd.add(end);
            	}
            } else {
            	if (Score == 0){
            		tmpStart.add(start);
            		tmpEnd.add(end);
            	} else {					// if (Score > 0)
            		greaterScFound = true;
            		tmpStart.clear();
            		tmpEnd.clear();
            		
            		tmpStart.add(start);
            		tmpEnd.add(end);
            	}
            }
        }
        posMovesStart.clear();
        posMovesEnd.clear();

        posMovesStart.addAll(tmpStart);
        posMovesEnd.addAll(tmpEnd);
    }
    
    
    
    public int calcThisColor(String curColor){
        int amount = 0;
        for (int row=0; row<8; row++) {
            for (int col = 0; col < 8; col++) {
                if (board[row][col].Color == curColor) {
                    amount += 1;
                }
            }
        }
        return amount;
    }


    public  void calcPieceScore(Piece curPiece, String curColor){

        curPiece.validPath.clear();
        curPiece.validMoves.clear();

        curPiece.bestMove = curPiece;
        curPiece.visitedCells.clear();
        boolean vM = curPiece.findValidMoves( this,curPiece.xPos, curPiece.yPos,  true, curColor);

        curPiece.visitedCells.clear();
        curPiece.calculateScore(this, curColor);

    }

    
    public  void makeMove(Board oldB, Piece curPiece, Piece Goal, String curColor){

        board = oldB.copyB();
        int x = curPiece.xPos;
        int y = curPiece.yPos;
        board[x][y].removePiece();

        int xGoal = Goal.xPos;
        int yGoal = Goal.yPos;

        if (curColor == "x") {
            board[xGoal][yGoal].makeX();
        } else {
            board[xGoal][yGoal].makeO();
        }

        if (curPiece.king){
            board[xGoal][yGoal].make_king();
        }

        if (xGoal == 0 || xGoal == 7){
            board[xGoal][yGoal].make_king();
        }

        curPiece.visitedCells.clear();
        curPiece.validPath.clear();
        boolean res = curPiece.findValidPath(this, curPiece.xPos, curPiece.yPos, true, Goal.xPos, Goal.yPos, curColor);
        for (Piece i : curPiece.validPath){
            board[i.xPos][i.yPos].removePiece();
        }
        //return this;
    }

    
    public  Piece[][] createEmptyBoard() {
        Piece[][] board = new Piece[8][8];
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                LinkedList<Piece> validMoves = new LinkedList<Piece>();
                board[row][col] = new Piece(row, col, " ", false, false,  false, validMoves);
            }
        }
        //printBoard();
        return board;
    }

    
    public  Piece[][] createBoard() {
        Piece[][] b = new Piece[8][8];
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                LinkedList<Piece> validMoves = new LinkedList<Piece>();
                Piece piece = new Piece(row, col, " ", false, false,  false, validMoves);

                if (((col % 2) + 2) % 2 == ((((row +  1) % 2) + 2) % 2)) {

                    if (row < 3) {
                        piece.changePiece(row, col, "o", false, false,  true);
                    } else if (row > 4) {
                        piece.changePiece(row, col, "x", false,true,  true );
                    } else {
                        piece.changePiece(row, col, " ", false, false,  false);
                    }
                }
                else{
                    piece.changePiece(row, col, " ", false, false,  false);
                }
                b[row][col] = piece;
            }
        }

        return b;
    }

    
    public  void printBoard(){
        System.out.println("     " + Arrays.toString(new int[]{0, 1, 2, 3, 4, 5, 6, 7}));
        System.out.println(" ");
        for (int i = 0; i < 8; i++) {
            ArrayList line = new ArrayList();


            for (int j = 0; j < 8; j++) {
                line.add(board[i][j].Color);

            }
            System.out.println(i + "    " + line);
        }
    }

    
    public  Piece[][] copyB(){
        Piece[][] boardCopy = new Piece[8][8];
        for (int it = 0; it<8; it++){
            for (int jt=0; jt<8; jt++){

                Piece oldP = board[it][jt];
                Piece copyP = new Piece(oldP.xPos, oldP.yPos, oldP.Color, oldP.king, oldP.upDirection,  oldP.activeStatus, oldP.validPath);
                boardCopy[it][jt] = copyP;
            }
        }
        return boardCopy;
    }

    
    public  int calcKings(String color){
        int amount = 0;
        for (int row=0; row<8; row++) {
            for (int col = 0; col < 8; col++) {
                if (board[row][col].Color == color && board[row][col].king) {
                    amount += 1;
                }
            }
        }
        return amount;
    }

    
    public String chooseHumCol(String KUKA_color){
        String HumanColor = "x";
        if (KUKA_color == HumanColor){
            HumanColor = "o";
        }
        return HumanColor;
    }

    
    public boolean gameEnd(){
        int amountX = calcThisColor("x");
        int amountO = calcThisColor("o");
        if (amountX == 0 || amountO == 0){
            return true;
        }
        return false;
    }

    
    public int KukaWins(String KukaColor){
        String OtherColor = chooseHumCol(KukaColor);

        int amountX = calcThisColor(KukaColor);
        if ((amountX == 0) || playerCannotMove(KukaColor)){
            return -100;
        }
       
        int amountO = calcThisColor(OtherColor);
        if ((amountO == 0) || playerCannotMove(OtherColor)){
            return 100;
        }
        return 0;
    }


    private boolean playerCannotMove(String color){
        possibleMoves(color);
        if (posMovesStart.size() == 0) {
            return true;
        }
        return false;
    }


    public int findThreats(String color){
        int numOfThreats = 0;
        String otherColor = chooseHumCol(color);
        possibleMoves(otherColor);
        for (int i = 0; i < posMovesStart.size(); i++) {
            Piece start = posMovesStart.get(i);
            Piece Goal = posMovesEnd.get(i);
            start.visitedCells.clear();
            start.validPath.clear();
            boolean res = start.findValidPath(this, start.xPos, start.yPos, true, Goal.xPos, Goal.yPos, otherColor);
            numOfThreats += start.validPath.size();
        }
        return numOfThreats;
    }
}
