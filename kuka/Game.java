package application;

import java.util.*;

import javax.inject.Inject;

import com.kuka.generated.ioAccess.PLC_ControlIOGroup;
import com.kuka.roboticsAPI.applicationModel.IApplicationData;
import com.kuka.roboticsAPI.uiModel.ApplicationDialogType;

public class Game {
	
    public static int actWhites	;
    public static int actBlacks	;
    public static int whiteKings;
    public static int blackKings;
    
    public static Board curB 			= new Board();
    public static Board prevB 		= new Board();

    public static ArrayList<Integer> pickCoordX		= new ArrayList<Integer>();
    public static ArrayList<Integer> pickCoordY		= new ArrayList<Integer>();
	public static ArrayList<Integer> placeCoordX	= new ArrayList<Integer>();
	public static ArrayList<Integer> placeCoordY	= new ArrayList<Integer>();
	
	public static int piToCaptLeft;
	
	public static boolean gameEnd;
	public static boolean KukaWins;
	public static boolean HumanWins;
	public static boolean moveWasMade;
	public static boolean turnIntoKing;
	
	public static String  currColor  = "o";
	public static String  humColor	 = "x";
	
	/** 
	 * Initializes the game parameters and sets them to default values.
	 */
	public static void initGame(){
		actBlacks 	 = 	12;
		actWhites    = 	12;
		whiteKings	 = 	0;
		blackKings	 =	0;
		
		gameEnd		 = false;
		KukaWins 	 = false;
		HumanWins 	 = false;
		moveWasMade  = false;
		turnIntoKing = false;
		
		prevB		 = new Board();
		prevB.board  = prevB.createBoard();
		piToCaptLeft = 0;
	}
	
	
	/**
	 * Analyzes the captured state of the board and checks if the player made a right move.
	 * 
	 * @param boardIO		board representing the current state of the game;
	 * @param start			true if it is the very first move of the game;
	 * @param difficulty	difficulty level of the game;
	 * @param KukaStarts	true if robot has white piece and thereby starts the game, false otherwise;
	 * @return				false if the opponent player made a wrong move or the end of 
	 * 						the game has been reached, true otherwise;
	 */
	public static boolean analizeBoard(int[][] boardIO, boolean start, int difficulty, boolean KukaStarts){
		
		
		fillCurBoard(boardIO, KukaStarts);
		
		if (!validPlayersMove(start, KukaStarts)){ return false; }
		
		if (checkGameEnd()){ return false; }		
		
		findMoveAI(difficulty);
		
		makeMove(boardIO);
		
		return true;
	}	
	
	
	/**
	 * Sets the sequence of actions the KUKA needs to make in order to make a move.
	 * 
	 * @param boardIO	board representing the current state of the game;
	 */
    public static void makeMove(int[][] boardIO){
    	pickCoordX.clear();
    	pickCoordY.clear();
    	placeCoordX.clear();
    	placeCoordY.clear();
  

    	processMove();
    	
    	
    	whiteKings = curB.calcKings(currColor);
		blackKings = curB.calcKings(humColor);
		
		prevB.board = copyB();
    }
   
    private static void processMove(){
    	if (curB.posMovesStart.size() == 0){
            System.out.println("No moves available. Game end");
            gameEnd = true;
            HumanWins = true;
            return;
        }
    	
    	System.out.println("Valid move starts: ");
        System.out.println("X: " + AI.bestMoveStart.xPos + ", Y: " + AI.bestMoveStart.yPos);
        System.out.println("Valid move ends: ");
        System.out.println("X: " + AI.bestMoveEnd.xPos + ", Y: " + AI.bestMoveEnd.yPos);
        
        Piece start = AI.bestMoveStart;
        Piece end = AI.bestMoveEnd;
        
        System.out.println("FIRST CHECK FOR King:     " + start.king);
        
        pickCoordX.add(start.xPos);
		pickCoordY.add(start.yPos);
		
		
		
        if ((end.xPos == 0 || end.xPos==7) && !start.king) {

        	curB.board[end.xPos][end.yPos].makeO();
    		curB.board[end.xPos][end.yPos].make_king();
    		turnIntoKing = true;
    		piToCaptLeft = 2;
    		//whiteKings += 1;
        } else {	
			curB.board[end.xPos][end.yPos].changePiece(end.xPos, end.yPos, currColor, false, false, true);
			if (start.king){
				curB.board[end.xPos][end.yPos].make_king();
			}
			piToCaptLeft = 1;
        }
        
        System.out.println("I'm done 1");
		
        placeCoordX.add(end.xPos);
		placeCoordY.add(end.yPos);
        
		start.validPath.clear();
        start.visitedCells.clear();
        System.out.println("King:     " + start.king);
        boolean vP = start.findValidPath(curB, start.xPos, start.yPos, true, end.xPos, end.yPos, currColor);
        actBlacks = actBlacks - start.validPath.size();
      
        curB.board[start.xPos][start.yPos].removePiece();
        
        System.out.println("Valid path:");
        while (start.validPath.size() > 0){
			int pX = start.validPath.getLast().xPos;
			int pY = start.validPath.getLast().yPos;
			System.out.println("X: " + pX + ", Y: " + pY);
			
			pickCoordX.add(pX);
			pickCoordY.add(pY);
			curB.board[pX][pY].removePiece();
			
			start.validPath.removeLast();
			placeCoordX.add(-1);	// to throw the piece into the bin
			placeCoordY.add(-1);
			
			piToCaptLeft = piToCaptLeft + 1;
		}
    }
    
    
  
    /**
     * Checks if the player move was valid by comparing the new state of the game with the old one.
     * 
     * @param start			is true if it is the very first move of the game;
     * @param KukaStarts	is true if robot has white piece and thereby starts the game, false otherwise;
     * @return				true if the move was valid.
     */
    public static boolean validPlayersMove(boolean start, boolean KukaStarts){
    	
    	if (playerCannotMove()){ return true; }
    	 	
        moveWasMade = true;
        
        ArrayList<Piece> KukasStart = new ArrayList<Piece>();
        ArrayList<Piece> KukasFinish = new ArrayList<Piece>();
        ArrayList<Piece> PlayersPiecesStart = new ArrayList<Piece>();
        ArrayList<Piece> PlayersPiecesFinish = new ArrayList<Piece>();
        
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (prevB.board[row][col].Color == currColor && curB.board[row][col].Color == " "){
                    KukasStart.add(prevB.board[row][col]);
                }
                if (prevB.board[row][col].Color == " " && curB.board[row][col].Color == currColor){
                    KukasFinish.add(prevB.board[row][col]);
                }
                if (prevB.board[row][col].Color == humColor && curB.board[row][col].Color == " "){
                    PlayersPiecesStart.add(prevB.board[row][col]);
                }
                if (prevB.board[row][col].Color == " " && curB.board[row][col].Color == humColor){
                    PlayersPiecesFinish.add(prevB.board[row][col]);
                }
            }
        }

        if (KukasFinish.size() > 0) { 
        	System.out.println("KukasFinish.size > KukasStart.size");
        	return false; 
        }
        if (PlayersPiecesStart.size() > 1) {
        	System.out.println("PlayersPiecesStart.size > 1");
        	return false; }
        if (PlayersPiecesFinish.size() > 1){
        	System.out.println("PlayersPiecesFinish.size() > 1");
        	return false; }
        if (PlayersPiecesStart.size() == 0) { 
        	System.out.println("PlayersPiecesStart.size() == 0");
        	moveWasMade = false;	
        	if (start && KukaStarts){
        		return true;
        	}
        	return false;}
        	//return true;}
        if (PlayersPiecesFinish.size() == 0) { 
        	System.out.println("PlayersPiecesFinish.size() == 0");
        	return false;}


        Piece curPieceS = PlayersPiecesStart.get(0);
        Piece curPieceF = PlayersPiecesFinish.get(0);
        
        
        if (curPieceF.becomesKing()){
        	if (!curB.board[curPieceF.xPos][curPieceF.yPos].king){
        		return false;
        	}
        }
        
        
        if (!moveIsPossible(curPieceS, curPieceF)){  return false; }
        
        if (!allPiecesCaptured(curPieceS, curPieceF, KukasStart)){	return false; }

        
        actWhites = actWhites - KukasStart.size();	

        return true;
    }
    
	/**
	 * Processes the board representation received from PLC via PROFINET and changes the data
	 * representation to the KUKA's one. Prepares the board representation for further usage and 
	 * saves it into global curB.
	 * 
	 * @param boardIO		board representing the current state of the game formated by PLC;
	 * @param KUKAstarts	is true if robot has white piece and thereby starts the game, false otherwise.
	 */
    public static void fillCurBoard(int[][] boardIO, boolean KUKAstarts){
    	if (KUKAstarts){
    		for (int row = 0; row < 8; row++) {
    			for (int col = 0; col < 8; col++) {
    				if (boardIO[row][col] == 1){
    					curB.board[row][col].changePiece(row, col, "x", false, true, true);
    					
    				} else if (boardIO[row][col] == 2){
    					curB.board[row][col].changePiece(row, col, "o", false, false, true);
    					
    				} else if (boardIO[row][col] == 3){
    					curB.board[row][col].changePiece(row, col, "x", true, true, true);
    					
    				} else if (boardIO[row][col] == 4){
    					curB.board[row][col].changePiece(row, col, "o", true, false, true);
    					
    				} else {
    					curB.board[row][col].removePiece();
    				}
    			}
    		}
    	} else {
    		for (int row = 0; row < 8; row++) {
    			for (int col = 0; col < 8; col++) {
    				if (boardIO[row][col] == 2){
    					curB.board[row][col].changePiece(row, col, "x", false, true, true);
    					
    				} else if (boardIO[row][col] == 1){
    					curB.board[row][col].changePiece(row, col, "o", false, false, true);
    					
    				} else if (boardIO[row][col] == 4){
    					curB.board[row][col].changePiece(row, col, "x", true, true, true);
    					
    				} else if (boardIO[row][col] == 3){
    					curB.board[row][col].changePiece(row, col, "o", true, false, true);
    					
    				} else {
    					curB.board[row][col].removePiece();
    				}
    			}
    		}
    	}
    }
    
   
    /**
     * Checks if the made move was in a list of possible moves for that board state.
     * 
     * @param curPieceS		coordinates of the move's starting position;
     * @param curPieceF		coordinates of the move's ending position;
     * @return				true if move was possible, false otherwise.
     */
    private static boolean moveIsPossible(Piece curPieceS, Piece curPieceF){
    	prevB.possibleMoves(humColor);
        boolean moveValid = false;
        for (int i = 0; i<prevB.posMovesStart.size(); i++){
        	Piece pStart = prevB.posMovesStart.get(i);
        	Piece pFinish = prevB.posMovesEnd.get(i);

        	if (curPieceS.xPos == pStart.xPos && curPieceS.yPos == pStart.yPos && curPieceF.xPos == pFinish.xPos && curPieceF.yPos == pFinish.yPos){
        		moveValid = true;
        	}	
        }
        if (!moveValid) { 
        	return false; }
        return true;
    }
    
    
    /**
     * Checks if all the opponents pieces placed on the path of the KUKA's move were captured.
     * 
     * @param curPieceS		coordinates of the move's starting position;
     * @param curPieceF		coordinates of the move's ending position;
     * @param KukasStart	is true if robot has white piece and thereby starts the game, false otherwise;
     * @return				true if all pieces were captured, false otherwise.
     */
    private static boolean allPiecesCaptured(Piece curPieceS, Piece curPieceF, ArrayList<Piece> KukasStart){
    	curPieceS.visitedCells.clear();
        curPieceS.validPath.clear();
        boolean vP = curPieceS.findValidPath(prevB, curPieceS.xPos, curPieceS.yPos, true, curPieceF.xPos, curPieceF.yPos, humColor);

        if (curPieceS.validPath.size() != KukasStart.size()) { return false; }
        for (int i = 0; i < KukasStart.size(); i++) {
            if (!KukasStart.contains(curPieceS.validPath.get(i))){ return false; }
        }
        return true;
    }
    
    

    /**
     * Copies the current state of the board and returns it.
     * 
     * @return	the copy of the board.
     */
    public static Piece[][] copyB(){
        Piece[][] boardCopy = new Piece[8][8];
        for (int it = 0; it<8; it++){
            for (int jt=0; jt<8; jt++){

                Piece oldP = curB.board[it][jt];
                Piece copyP = new Piece(oldP.xPos, oldP.yPos, oldP.Color, oldP.king, oldP.upDirection, oldP.activeStatus, oldP.validPath);
                boardCopy[it][jt] = copyP;
            }
        }
        printBoard(boardCopy);
        return boardCopy;
    }
    
    
    /**
     * Creates a board representation that corresponds to the beginning of the game.
     * 
     * @return	the created board.
     */
    public static Piece[][] createBoard() {
        Piece[][] board = new Piece[8][8];
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                LinkedList<Piece> validMoves = new LinkedList<Piece>();
                Piece piece = new Piece(row, col, " ", false, false, false, validMoves);

                if (((col % 2) + 2) % 2 == ((((row +  1) % 2) + 2) % 2)) {

                    if (row < 3) {
                        piece.changePiece(row, col, "o", false, false, true);
                    } else if (row > 4) {
                        piece.changePiece(row, col, "x", false,true, true );
                    } else {
                        piece.changePiece(row, col, " ", false, false, false);
                    }
                }
                else{
                    piece.changePiece(row, col, " ", false, false, false);
                }
                board[row][col] = piece;
            }
        }
        printBoard(board);
        return board;
    }

    
    public static void printBoard(Piece[][] board){
        System.out.println("     " + Arrays.toString(new int[]{0, 1, 2, 3, 4, 5, 6, 7}));
        System.out.println(" ");
        for (int i = 0; i < 8; i++) {
            ArrayList line = new ArrayList();


            for (int j = 0; j < 8; j++) {
                line.add(board[i][j].Color);

            }
            System.out.println(i + "    " + line);
        }
    }

    
    public static boolean playerCannotMove(){
        prevB.possibleMoves(humColor);
        if (prevB.posMovesStart.size() == 0) {
        	gameEnd = true;
    		KukaWins = true;
            return true;
        }
        return false;
    }
    
    
    private static void findMoveAI(int difficulty){
    	
    	piToCaptLeft = 0;
	
		switch (difficulty){
		case 1:
			AI.setSearchDepth(3);
			int res1 = AI.alphaBeta(curB, 3, -10000, 10000, true, currColor);
			break;
			
		case 2:
			AI.setSearchDepth(5);
			int res2 = AI.alphaBeta(curB, 5, -10000, 10000, true, currColor);
			break;
		case 3:
			AI.setSearchDepth(7);
			int res3 = AI.alphaBeta(curB, 7, -10000, 10000, true, currColor);
			break;
			
		default:
			break;
		}
	}
    
    
    
    private static boolean checkGameEnd(){
	
    	boolean end = false;
    	if (gameEnd){
    		end = true;
    	} 
    	if (curB.calcThisColor(humColor) == 0){
			gameEnd = true;
			KukaWins = true;
			end = true;
		} 
    	if (curB.calcThisColor(currColor) == 0){
			gameEnd = true;
			HumanWins = true;
			end = true;
		}
    	return end;
    }
    
}
