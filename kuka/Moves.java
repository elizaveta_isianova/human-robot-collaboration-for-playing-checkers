package application;

import static com.kuka.roboticsAPI.motionModel.BasicMotions.lin;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.ptp;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.spl;

import javax.inject.Inject;



import com.kuka.generated.ioAccess.PLC_ControlIOGroup;
import com.kuka.roboticsAPI.applicationModel.IApplicationData;
import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.geometricModel.CartDOF;
import com.kuka.roboticsAPI.geometricModel.Frame;
import com.kuka.roboticsAPI.geometricModel.ObjectFrame;
import com.kuka.roboticsAPI.motionModel.IMotionContainer;
import com.kuka.roboticsAPI.motionModel.PositionHold;
import com.kuka.roboticsAPI.motionModel.Spline;
import com.kuka.roboticsAPI.motionModel.controlModeModel.CartesianImpedanceControlMode;
import com.kuka.roboticsAPI.uiModel.ApplicationDialogType;

public class Moves {
    @Inject
    private LBR lbr;

    @Inject
    private IApplicationData Data;

    double vertVelocity = 100;
    double horizVelocity = 210;
    double highVelocity  = 230;
    double pieceSize = 7;
    public CartesianImpedanceControlMode cartImpCtrlMode = new CartesianImpedanceControlMode();
    IMotionContainer positionHoldContainer;
    public boolean isAtHome;

    private double x_kuka;
    private double y_kuka;
    private int z_offset = 7;


    public void initializeImp(){

        cartImpCtrlMode.parametrize(CartDOF.X, CartDOF.Y).setStiffness(3000.0);
        cartImpCtrlMode.parametrize(CartDOF.Z).setStiffness(3000.0);

    }

    public void goHome(boolean start){
        if (!isAtHome){
        	ObjectFrame frame = Data.getFrame("/PhotoPos");
        	
    		Frame currFrame = lbr.getCurrentCartesianPosition(lbr.getFlange());
    		
    		double heightDiff = frame.getZ() - currFrame.getZ();
    		if (heightDiff > 0){
    			holdPos();
    			standUp(heightDiff);
    		}
        	      
            lbr.move(lin(frame).setCartVelocity(highVelocity).setMode(cartImpCtrlMode));
            
            holdPos();
        }
    }


    public void standUp(double d){

        releaseHoldP();

        Frame targetFrame = lbr.getCurrentCartesianPosition(lbr.getFlange());
        targetFrame.setZ(targetFrame.getZ() + d);
        lbr.move(lin(targetFrame).setCartVelocity(vertVelocity).setMode(cartImpCtrlMode));
        System.out.println("Stand up done");
    }



    public void placeRegToBoard(int x, int y, int piecesLeft, boolean KukaStarts){

        standUp(40);

        Frame targetFrame = lbr.getCurrentCartesianPosition(lbr.getFlange());

        transferCoords(x, y);

        targetFrame = setBasePickFrame();

        lbr.move(lin(targetFrame).setCartVelocity(horizVelocity).setMode(cartImpCtrlMode));

        int z_Off = calcOffset(x,y);
        
        targetFrame.setZ(281 + z_Off);
        lbr.move(lin(targetFrame).setCartVelocity(vertVelocity).setMode(cartImpCtrlMode));
    }



    public void placeRegToStack(boolean KukaStarts, int numPieces){
        ObjectFrame targetFr = null;
        if (KukaStarts){
        	targetFr = Data.getFrame("/WR");
        } else {
        	targetFr = Data.getFrame("/BR");
        }

        double newZ = 336.3767 - 7.2062 * (numPieces) + 10 ;

        place(targetFr, newZ);
    }


    public void placeEatenRegToStack(boolean KukaStarts, int numPieces){
        ObjectFrame targetFr = null;
        if (KukaStarts){
        	targetFr = Data.getFrame("/BR");
        } else {
        	targetFr = Data.getFrame("/WR");
        }

        double newZ = 336.3767 - 7.2062 * numPieces + 10;

        place(targetFr, newZ);
    }

    public void placeKingToStack(boolean KukaStarts, int numKings){
        ObjectFrame targetFr = null;
        if (KukaStarts){
        	targetFr = Data.getFrame("/BK");
        } else {
        	targetFr = Data.getFrame("/WK");
        }

        double newZ = 346.82 - 7.69 * numKings + 10;

        place(targetFr, newZ);
    }

    public void place(ObjectFrame targetFr, double newZ){
        standUp(40);

        lbr.move(lin(targetFr).setCartVelocity(horizVelocity).setMode(cartImpCtrlMode));

        Frame targetFrame = lbr.getCurrentCartesianPosition(lbr.getFlange());

        targetFrame.setZ(newZ + z_offset);
        System.out.println("Going to Z: " + newZ);
        
        lbr.move(lin(targetFrame).setCartVelocity(vertVelocity).setMode(cartImpCtrlMode));
        
    }



    public void pickFromBoard(int x, int y){

        transferCoords(x, y);

        Frame targetFrame = setBasePickFrame();
        lbr.move(lin(targetFrame).setCartVelocity(horizVelocity).setMode(cartImpCtrlMode));
        
        int z_Off = calcOffset(x,y);
        targetFrame.setZ(281 + z_Off);
        lbr.move(lin(targetFrame).setCartVelocity(vertVelocity).setMode(cartImpCtrlMode));


    }

    
    public void pickKingFromStack(int numKings, boolean KukaStarts){

        ObjectFrame targetFr;
        if (KukaStarts){
            targetFr = Data.getFrame("/WK");
        } else {
        	targetFr = Data.getFrame("/BK");
        }

        lbr.move(lin(targetFr).setCartVelocity(horizVelocity).setMode(cartImpCtrlMode));
        Frame targetFrame = lbr.getCurrentCartesianPosition(lbr.getFlange());

        double newZ = 346.82 - 7.69 * numKings + 13;
        
        System.out.println("White kings left: " + numKings);
        System.out.println("Going to Z: " + newZ);

        
        targetFrame.setZ(newZ);
        lbr.move(lin(targetFrame).setCartVelocity(vertVelocity).setMode(cartImpCtrlMode));
    }


    public Frame setBasePickFrame(){
        Frame targetFrame = new Frame(x_kuka, y_kuka, 317, -2.286, -0.01745, 3.13);
        return targetFrame;
    }

    
    private void transferCoords(int x, int y){
        x_kuka = x*23.5 + y*20.6 + 435.4;
        y_kuka = x*(-21.1) + y*23.4  - 50.9;
    }
    

    public void holdPos(){
        positionHoldContainer = lbr.moveAsync((new PositionHold(cartImpCtrlMode, -1, null)));
    }


    public void releaseHoldP(){
        while (true){
            positionHoldContainer.cancel();
            if (positionHoldContainer.isFinished()){ break; }
        }
    }

    
    private int calcOffset(int x, int y){
    	int z;
    	if (x <= 4 && y <=4){
    		z = 7;
    	} else {
    		z = 4;
    	}
    	return z;
    }
    

    public boolean removePiece(int x){
        if (x<0){
            return true;
        }
        return false;
    }
}


