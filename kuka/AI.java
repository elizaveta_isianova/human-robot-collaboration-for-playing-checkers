package application;

import java.util.LinkedList;

public class AI {
	/* for greedy */
    public static String myColor;
    public static int bestScore;
    public static int[] move = new int[] {-1,-1};
    public static LinkedList<Piece> path;
    public static Piece startPiece;
    
    /* for minimax */
    public static Piece bestMoveStart;
    public static Piece bestMoveEnd;

    
    static int pieceCoef	= 	10;
    static int kingCoef 	= 	20;
    static int edgeCoef 	= 	6;
    static int thrCoeff 	=  -10;
   
    static int MAX_DEPTH ;
    
    static boolean tmpFLAG = false;
    
    
   
    
    /**
     * Implementation of the Minimax algorithm with the Alpha-Beta Pruning.
     * It calculates the best move for the current state of the game by analyzing 
     * all the possible game states after N (N = depth) moves. 
     * Returns the score of the current game state and stores coordinates
     * of the position, where a piece should be picked in bestMoveStart and where
     * it should be placed in bestMoveEnd.
     * 
     * @param b			 current state of the board
     * @param depth		 searching depth of the algorithm
     * @param alpha		 
     * @param beta
     * @param maximizing
     * @param curColor	 color of the current player's pieces 
     * @return
     */
    public static int alphaBeta(Board b, int depth, int alpha, int beta, boolean maximizing, String curColor) {
        
        String otherColor = b.chooseHumCol(curColor);

        boolean gEnd = b.gameEnd();
        if (depth == 0 || gEnd) {
            int eval = heuristicEval(b, curColor, gEnd, depth);
            return eval;
        }

        if (maximizing) {
            int maxEval = -1000;
            b.possibleMoves(curColor);
            if (depth == MAX_DEPTH && b.posMovesStart.size() == 1){
                bestMoveStart = b.posMovesStart.get(0);
                bestMoveEnd = b.posMovesEnd.get(0);
                return 777;
            }
            for (int i = 0; i < b.posMovesStart.size(); i++) {
                Board newB = new Board();
                newB.makeMove(b, b.posMovesStart.get(i), b.posMovesEnd.get(i), curColor);
                int mL = b.posMovesStart.get(i).validPath.size();
                int eval = alphaBeta(newB, depth - 1, alpha, beta, false, curColor);
                if (eval > maxEval) {
                    maxEval = eval;

                    if (depth == MAX_DEPTH) {
                        bestMoveStart = b.posMovesStart.get(i);
                        bestMoveEnd = b.posMovesEnd.get(i);
                    }
                }
                if (maxEval > alpha) { alpha = maxEval; }
                if (eval >= beta)    { break; }
            }
            return maxEval;
        } else {
            int minEval = 1000;
            b.possibleMoves(otherColor);
            for (int i = 0; i < b.posMovesStart.size(); i++) {
                Board newB = new Board();
                newB.makeMove(b, b.posMovesStart.get(i), b.posMovesEnd.get(i), otherColor);
                int eval = alphaBeta(newB, depth - 1, alpha, beta, true, curColor);

                if (eval < minEval) { minEval = eval; }
                
                if (minEval < beta) { beta = minEval; }
                
                if (beta <= alpha)  { break; }
            }
            return minEval;
        }
    }

    /**
     * Evaluates the current state of the game and returns the corresponding score.
     * 
     * @param b			current state of the game
     * @param curColor  color of the current player's pieces 
     * @param GameEnd	is true if game end has been reached
     * @param depth		represents a number of moves that were made
     * @return			the calculated score
     */
    private static int heuristicEval(Board b, String curColor, boolean GameEnd, int depth){

        String otherColor = b.chooseHumCol(curColor);
        int numThisOnEdge = 0;
        int numNotThisOnEdge=0;

        for (int i=0;i<8;i++){
            for (int j=0;j<8;j++){
                if (b.board[i][j].Color == curColor){
                    if (b.board[i][j].onEdge()){
                        numThisOnEdge += 1;
                    }
                }
                if (b.board[i][j].Color == otherColor){
                    if (b.board[i][j].onEdge()){
                        numNotThisOnEdge += 1;
                    }
                }
            }
        }

        int endGameCoef = 0;
        if (GameEnd) {
            endGameCoef = b.KukaWins(curColor);
        }

        int thisColNum = b.calcThisColor(curColor) - numThisOnEdge;
        int notThisColNum = b.calcThisColor(otherColor) - numNotThisOnEdge;

        int thisKings = b.calcKings(curColor);
        int notThisKings = b.calcKings(otherColor);

        int thisThreats = b.findThreats(curColor);
        int notThisThreats = b.findThreats(otherColor);

        int eval =  endGameCoef + depth*2 + (thisColNum - notThisColNum)*pieceCoef + (thisKings - notThisKings)*kingCoef +  (thisThreats - notThisThreats)*thrCoeff + (numThisOnEdge - numNotThisOnEdge)*edgeCoef;

        return eval;
    }

    /**
     * Sets the searching depth of the Alpha Beta pruning algorithm.
     * 
     * @param depth
     */
    public static void setSearchDepth(int depth){
    	MAX_DEPTH = depth;
    }

}


