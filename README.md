
<h1 align="center">Welcome to my Bachelor thesis - Human-robot collaboration for playing
checkers 👋</h1>

## ✨ Description

This thesis aims to create a collaborative robotic workplace for playing the game checkers with a human opponent. The robotic workplace meets `industrial standards` and has a convenient user interface. The architecture of the system, consisting of a `PLC`, `collaborative robot`, and a `computer vision system`, allows the robot to be fully autonomous in the perception of the checkerboard and in playing the game. The game algorithm is extended with `artificial intelligence` that contributes to fast decision making and the robot's high win rate.

The principle of the Human-Robot collaboration implemented on the example of the checkers game can later be integrated into any other application of industrial robots. 


<p align="center">
  <img src="photos/real_thing.JPG?raw=true" height="600" title="hover text">
</p>



## 🚀 Project architecture 

The main system controller linking the rest of the elements is the PLC `Siemens CPU 1510SP F-1 PN`. The `KUKA iiwa LBR` robot, the HMI screen `Siemens TP900 comfort`, and the gripper `SCHUNK EGL` are connected to PLC through the `Profinet` industrial communication protocol. The computer vision (CV) system is represented with the `Keyence Vision System`, although initially, the CV tasks were computed in `Python` using the `OpenCV` library with the help of an industrial camera.

<p align="center">
  <img src="photos/2.png?raw=true" height="400" title="hover text">
</p>


### 🔑  Application structure

The concept of this robotic workplace was kept to the idea of a `"plug and play"` system whose functionality does not require any manual setups or any operator involvement. The launch of the game takes only two button presses on the touch panel. A player can choose between two checker colors and three difficulty levels. The robot is set in `collaborative mode`, so the risks of accidents caused by a collision of a robot with a human are negligible. In case of unexpected behavior, the robot can be stopped immediately with an emergency button.

The simplified version of the system's `state machine` is shown below.

<p align="center">
  <img src="photos/3.png?raw=true" height="900" title="hover text">
</p>



The program for PLC and HMI is implemented in `SCL` and `LAD` programming languages in Step 7 `TIA Portal`. 

The handling of the checkers game and the control over the robot's motions are implemented in the robot `Java` application. It was developed in `KUKA Sunrise.Workbench`, and it runs by KUKA `Sunrise.Cabinet` and `KUKA iiwa` itself.

Computer vision tasks were initially performed on the PC in `Python` using the `OpenCV` library. In the final version of the project, the CV module was replaced with the Keyence Vision System, where programming is done by the GUI interface.

## 💻 Artificial intelligence

The implemented game algorithm is based on `Minimax` with `Alpha-Beta pruning`. The depth of the searching tree expansion corresponds to the difficulty level of the game: 
- 3 corresponds to `Easy` level;
- 5 corresponds to `Medium` level;
- 7 corresponds to `Hard` level.

At the end of the expansion of the searching tree, the score of the state is calculated at every child node as an output of the `heuristic function`. The heuristic function is calculated as a linear combination of such parameters as the number of the current player's and its opponent's regular pieces and kings present on the board and their location on it. 


## 📝 System tests

The robot workplace tests have shown great performance in interacting with the general public. The choice of parameters for the heuristic function of a Minimax algorithm was proper, and thus the AI player was almost invincible in difficulty modes higher than the easiest possible. 
The tests were held with the help of 10 participants aged 20 to 55 years with different backgrounds. While most of them were familiar with robotic systems, there were four people who had never interacted with robots before. The results are shown below.

<p align="center">
  <img src="photos/4.png?raw=true" height="270" title="hover text">
</p>

### 💻 Organizations

The work on this project has been carried out as part of an internship at the Testbed for Industry 4.0 at the Czech Institute of Informatics, Robotics and Cybernetics (CIIRC CTU).

<a href="https://www.ciirc.cvut.cz/"><img src="https://www.ciirc.cvut.cz/wp-content/uploads/2017/10/logo_CIIRC_en.svg" height="50"  > </a>


## 🤝 Contributing

I would like to thank Ing. Pavel Burget Ph.D for supervising this work. I thank Ing. Tomáš Jochman for consultations on PLC and KUKA programming. I thank Serhii Voronov for the help in assembling the robotic workplace and 3D printing of parts.
I would also like to thank the Czech Institute of Informatics, Robotics and Cybernetics for providing an opportunity to work on this project.<br />

## 👩‍🎓  Author

 **Elizaveta Isianova**

- Github: [@elizaveta_isianova](https://gitlab.com/elizaveta_isianova)

## 🫶 Show your support

Please ⭐️ this repository if this project helped you!

## 📝 License

Copyright © 2022 [Elizaveta Isianova](https://gitlab.com/elizaveta_isianova).<br />


---
